#!/bin/bash
set -e
export PATH=/bin:/sbin:/usr/bin:/usr/sbin
root_passwd=""
user_name=""
user_passwd=""

OUTPUT="/tmp/input.txt"
# create empty file
>$OUTPUT



###### Main Menu ######
set_regional_settings(){
echo root passwd $root_passwd
echo user name $user_name
echo user passwd $user_passwd

    while true ; do
        result=$(dialog --no-cancel \
            --output-fd 1 --menu \
            "regional settings Menu" 0 0 0 \
            "1" "Default Turkish q Keyboard Install" \
            "2" "Default Turkish f Keyboard Install" \
            "3" "Default English q Keyboard Install" \
            "4" "Custom Language and Keyboard Setting" \
            "5" "Run Terminal" \
            "0" "Reboot"
        )
        echo -ne "\033c"
        if [[ $result -eq 0 ]] ; then
            reboot -f
        elif [[ $result -eq 1 ]] ; then
            echo ""
        elif [[ $result -eq 2 ]] ; then
            echo ""
        elif [[ $result -eq 3 ]] ; then
            echo ""
       	elif [[ $result -eq 4 ]] ; then
            echo ""
        elif [[ $result -eq 5 ]] ; then
           echo -e "\033[33;1mTerminal mode. You can type exit and go back.\033[;0m"
            PS1="\033[32;1m>>>\033[;0m " /bin/busybox ash
        fi
        echo $result
        read -n 1
    done
}
#### set user passwd ####
set_user_passwd(){
echo "">$OUTPUT
dialog  --no-cancel --title "set user passwd" \
--backtitle "initial settings" \
--inputbox "enter user passwd " 0 0 2>$OUTPUT

if [ -f "${OUTPUT}" ];then
    if [ -s "${OUTPUT}" ];then
        #echo "File ${OUTPUT} exists and not empty"
        user_passwd=$(<$OUTPUT)
	set_regional_settings
        #echo user passwd $user_passwd
    else
        #echo "File ${OUTPUT} exists but empty"
        set_user_passwd
    fi
else
    echo "File ${OUTPUT} not exists"
fi
}

#### create new user ####
create_new_user(){
echo "">$OUTPUT
dialog  --no-cancel --title "set new  user" \
--backtitle "initial settings" \
--inputbox "enter new username " 0 0 2>$OUTPUT

if [ -f "${OUTPUT}" ];then
    if [ -s "${OUTPUT}" ];then
        #echo "File ${OUTPUT} exists and not empty"
        user_name=$(<$OUTPUT)
        set_user_passwd
        #echo username $user_name
    else
        #echo "File ${OUTPUT} exists but empty"
        create_new_user
    fi
else
    echo "File ${OUTPUT} not exists"
fi
}

#### set root passwd ####
set_root_passwd(){

dialog  --no-cancel --title "set root password" \
--backtitle "initial settings" \
--inputbox "enter root password " 0 0 2>$OUTPUT

if [ -f "${OUTPUT}" ];then
    if [ -s "${OUTPUT}" ];then
        #echo "File ${OUTPUT} exists and not empty"
        root_passwd=$(<$OUTPUT)
        create_new_user
        #echo root passwd $root_passwd
    else
        #echo "File ${OUTPUT} exists but empty"
        set_root_passwd
    fi
else
    echo "File ${OUTPUT} not exists"
fi

}


set_root_passwd
